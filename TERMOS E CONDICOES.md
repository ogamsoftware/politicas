
Termos e Condições de Uso do Aplicativo Harca

1. Objeto

Estes Termos e Condições de Uso ("Termos") regem o uso do aplicativo SaaS Android "Harca" ("Aplicativo") desenvolvido por Ogam Software Soluções Inteligentes ("Desenvolvedor") e disponibilizado para download e uso no Brasil. Ao utilizar o Aplicativo, o usuário ("Usuário") concorda com os presentes Termos.

2. Descrição do Aplicativo

O Harca é uma ferramenta SaaS que oferece aos usuários a funcionalidade de contatar médicos veterinários através do aplicativo. E aos médicos veterinários autônomos a possibilidade de gerenciar sua rotina de trabalho. O Aplicativo requer acesso à internet para funcionar.

3. Licença de Uso

O Desenvolvedor concede ao Usuário uma licença pessoal, não exclusiva e intransferível para usar o Aplicativo, de acordo com os presentes Termos.

4. Restrições de Uso

O Usuário não poderá:

Modificar, copiar, distribuir, transmitir, vender, alugar, sublicenciar ou de qualquer forma explorar o Aplicativo;
Tentar obter acesso não autorizado ao Aplicativo ou a seus sistemas;
Usar o Aplicativo para fins ilegais ou que violem os direitos do Desenvolvedor ou de terceiros.

5. Propriedade Intelectual

Todos os direitos de propriedade intelectual sobre o Aplicativo Harca, incluindo sua marca, código-fonte, design e conteúdo, pertencem ao Desenvolvedor.

6. Responsabilidade

O Desenvolvedor não se responsabiliza por:

Interrupções ou falhas no funcionamento do Aplicativo;
Danos causados por vírus ou malware;
Perdas ou danos causados pelo uso indevido do Aplicativo.

7. Privacidade

O Desenvolvedor se compromete a proteger a privacidade do Usuário. A Política de Privacidade do Harca, disponível em https://politicaprivacidade.com/, explica como os dados do Usuário são coletados e utilizados.

8. Prazo e Rescisão

A licença de uso do Aplicativo Harca é válida por tempo indeterminado. O Desenvolvedor poderá rescindir a licença a qualquer momento, mediante notificação ao Usuário.

9. Disposições Gerais

Os presentes Termos serão regidos pelas leis do Brasil. Fica eleito o foro da Comarca de São Paulo para dirimir qualquer litígio.

10. Contato

Em caso de dúvidas ou sugestões, o Usuário poderá entrar em contato com o Desenvolvedor através do e-mail ogam@ogam.com.br.

Ao utilizar o Aplicativo Harca, o Usuário declara ter lido e concordado com os presentes Termos e Condições de Uso.